# KN03 Datenmanipulation und Abfragen I

## A) Daten hinzufügen

> siehe 'insert-mongo.js'

## B) Daten löschen 

> siehe 'cleanup-mongo.js' und 'delete-mongo.js'

## C) Daten abfragen

> siehe 'read-mongo.js'

## D) Daten verändern

> siehe 'update-mongo.js'
