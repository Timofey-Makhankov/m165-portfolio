db.customers.updateOne({_id: ObjectId('666f202c3ea1782e20a26a20')}, {$set: {password: '12345678'}})
db.orders.updateMany({$or: [{orderCompleted: true}, {invoice: { price: {$gt: 400} }}]}, {$set: {orderCompleted: false}})
db.products.replaceOne({name: 'Smart Watch'}, {
    name: 'Apple Smart Watch',
    price: 249.5,
    description: 'Wearable Computer on your wrist from Apple'
})