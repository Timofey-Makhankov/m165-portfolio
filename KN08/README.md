# KN08: Datenabfrage und -Manipulation

## A) Daten hinzufügen

Siehe 'create-neo.cql'

## B) Daten abfragen

finde alle Bestellungen, die von 2024 kommen

```sql
MATCH (o:Order)
WHERE o.orderDate >= date('2024-01-01')
RETURN o
```

finde alle Invoices, die nicht bezahlt sind

```sql
MATCH (i:Invoice)
WHERE i.payDate IS NULL
RETURN i;
```
finde alle Kunden und zeige nur das Id und die Email des Kundens (Abfragung für Backend)

```sql
MATCH (c:Customer)
RETURN c.id, c.email
```
Siehe, wie viele Produkte schon verkauft haben

```sql
MATCH (:Order)-[:hasProducts]->(p:Product)
RETURN COUNT(p);
```

mit OPTIONAL kann man angeben, dass vielleicht gibt es keine Werte, es sollte aber trotzdem null Werten zeigen. Wenn OPTIONAL nich angegeben wird, wird es strikt und wird keine null werten zeigen

## C) Daten löschen

```sql
MATCH (n:Customer) WHERE n.email = 't@g.com' DELETE n;
```

```sql
MATCH (n:Customer) WHERE n.email = 't@g.com' DETACH DELETE n;
```

![](./img/Screenshot%202024-07-01%20142853.png)

Da ein Node noch Knoten hat, konnte man es nicht löschen. mit Detach wird es den Node aber auch alle Knoten, dass es verbunden ist

## D) Daten verändern

Das Passwort wurde verädert

```sql
MATCH (c:Customer {email: 'noa@noseryoung.ch'})
SET c.password = 'INoa'
```

Ein Invoice wurde aktualisiert und wurde bezahlt

```sql
MATCH (i:Invoice {refNumber: 897234657})
SET i.payDate = date('2024-07-01')
```

Der Preis vom Produkt wurde aktualisiert

```sql
MATCH (p:Product {id: "b65415f3-fa7c-40c9-a44d-e99019dc3d0e"})
SET p.price = 29.95
```

## E) Zusätzliche Klauseln

Das klausel REMOVE kann ein key im node oder knoten entfernen. In diesem beispiel wurde das key from node weggenommen, da es schon im knoten existerte

```sql
MATCH (i:Invoice {refNumber: 897234657})
REMOVE i.completed
```
Man kann die Resultate auch ordnen. Hier Ordner is alle bestellungen von neuesten runter

```sql
MATCH (o:Order)
RETURN o
ORDER BY o.orderDate DESC;
```

