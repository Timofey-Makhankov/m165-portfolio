# KN07: Installation und Datenmodellierung für Neo4j

## A) Installation / Account erstellen

![](./img/Screenshot%202024-06-17%20143811.png)

## B) Logisches Modell für Neo4j

![](./img/Screenshot%202024-07-01%20155903.png)

Ich habe die Arrays ausgeblendet im Neo4J, da die Knoten die Listen sind. zwischen Products und Orders habe ich beim Knoten den Wert gespeichert, da die Verbindung kann zeigen, ob wann das Produkt in der bestellung dazugegeben wurde. Die restlichen Werte konnte ich wie im MongoDB in Nodes abspeichern.