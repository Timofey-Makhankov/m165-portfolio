# KN05: Administration von MongoDB

## A) Rechte und Rollen

![](./img/Screenshot%202024-06-16%20194627.png)

```js
use shop

db.createUser({
    user: 'user_1',
    pwd: '1234',
    roles: [
        {role: "read", db: "shop"}
    ]
})
```

```js
use admin

db.createUser({
    user: 'user_2',
    pwd: '1234',
    roles: [
        {role: 'readWrite', db: 'shop'}
    ]
})
```

![](./img/Screenshot%202024-06-16%20205323.png)

![](./img/Screenshot%202024-06-16%20205420.png)

![](./img/Screenshot%202024-06-16%20205721.png)

![](./img/Screenshot%202024-06-16%20205729.png)

![](./img/Screenshot%202024-06-16%20205844.png)

![](./img/Screenshot%202024-06-16%20210544.png)

![](./img/Screenshot%202024-06-16%20210736.png)

## B) Backup und Restore

![](./img/Screenshot%202024-06-17%20131729.png)

![](./img/Screenshot%202024-06-17%20133747.png)

![](./img/Screenshot%202024-06-17%20134049.png)

![](./img/Screenshot%202024-06-17%20134915.png)

![](./img/Screenshot%202024-06-17%20135154.png)

![](./img/Screenshot%202024-06-16%20211332.png)

![](./img/Screenshot%202024-06-16%20211413.png)

![](./img/Screenshot%202024-06-16%20211549.png)

![](./img/Screenshot%202024-06-16%20211727.png)

![](./img/Screenshot%202024-06-16%20211753.png)

## C) Skalierung

[Quelle](https://www.mongodb.com/resources/basics/scaling)

### Replication (Replikation)

Replikation ist der weg, um eine Datenbank zu duplizieren. Dass erlaubt, dass es Datensicherheit und verfügbar ist. Das schwierige an dem ist, dass die daten könnten entweder inconsistent werden oder wenn es nur ein Master DB gibt, kann der Server mit vielen Request verlangsamt.

![Replication Image](./img/lly2dso5vg9pytd6i-image5.jpg)

### Partition (Sharding)

Sharding erlaubt es, die einzelnen Tabellen / Collections in meherer kleineren Tabellen / Collections in verschiedene Datenbanken unterteilen. Die schwierigkeit kommt dann, wenn ein Request ankommt. Da muss man herausfinden, wo sollte dieses Request an welches Datenbank verschickt werden. Da gibt es so genanntes 'Router' und bearbeitet die Request und unterteilt sie an den Datenbanken. Vom Aufbau ist es viel mehr komplizierter als Replikation, aber kann viel mehr Request aufeinmal ausführen

![Sharding Image](./img/lly1yiwh4ydk2oy7b-image3.png)

### Empfehlung an einer Firma

Wenn eine Sozialen-media Firma mir ankommt und fragt, wie können wir unser Business skalieren, würde ich ihnen MongoDB mit Sharding empfehlen, da es mit vielen Users zu tun hat und es ist nicht ganz wichtig, die aller neuesten Daten zu zeigen.

Wenn eine E-Commerce Firma ankommen würde, würde ich MongoDB mit Replikation empfehlen, da es verhindert Datenverlust und erleichert das skaling, aber mit der sicherheit, dass alles consistent bleibt
