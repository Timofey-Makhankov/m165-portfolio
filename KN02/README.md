# KN02: Datenmodellierung für MongoDB

## A) Konzeptionelles Datenmodell

![](./img/KDM.svg)

In diesem Modell hat es vier entitäten. Es gibt eine one-to-many Beziehung zwischen 'Customers' und 'Orders'. es hat eine Many-to-Many Beziehung zwischen 'Orders' und 'Products'. Es gibt da auch eine One-to-One Beziehung zwischen 'Orders' und 'Invoices'

'Customers' und 'Products' Tabellen sind selbst erklärend. 'Orders' Tabelle enthält die Customer Id und liste von products id

## B) Logisches Modell für MongoDB

![](./img/LGM.svg)

bei der 1 zu 1 beziehung habe ich das Invoice im Orders verschachtelt, da es nur dort referenziert wird und das invoice wird immer mit einem Order verknüpft.

## C) Anwendung des Schemas in MongoDB

```js
use shop

db.createCollection("customers")
db.createCollection("orders")
db.createCollection("products")
```

![](./img/Screenshot%202024-05-27%20142127.png)
