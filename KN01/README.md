# KN01: Installation und Verwaltung von MongoDB

## A) Installation

![](./img/Screenshot%202024-05-13%20150251.png)

mit authpass sage ich, welche collection für authentication benutzt wird, bei default ist es das 'admin' collection

![](./img/Screenshot%202024-05-13%20151922.png)

das sed Befehl sucht ein text Abschnitt und überschreibt es mit einem neuen Wert. Da muss man nicht manuel ein Text in der Datei bearbeiten

mit 0.0.0.0 sage ich mongodb, dass es auf alle netwerke sichtbar sein sollte, mit 127.0.0.1 ist es nur für den Gerät sichtbar (localhost)

mit dem mongodb script soll es security aktivieren

## B) Erste Schritte GUI

```js
db.timofey.insert({
  "name": "Timofey Makhankov",
  "geburtsdatum": new Date("2006-01-23"),
  "groesse": 175.5,
  "Addresse": "lorem ipsum 12",
  "plz": 8001,
  "ort": "Zürich"
});
```
![](./img/Screenshot%202024-05-13%20153214.png)

Date ist als string und timestamp format gespeichert, _id habe ich selber generieren lassen, sonst müsste ich `new ObjectId()` aufrufen.

## C) Erste Schritte Shell

![](./img/Screenshot%202024-05-13%20153835.png)

![](./img/Screenshot%202024-05-13%20153858.png)

![](./img/Screenshot%202024-05-13%20153921.png)

![](./img/Screenshot%202024-05-13%20153936.png)

![](./img/Screenshot%202024-05-13%20153951.png)

![](./img/Screenshot%202024-05-13%20154240.png)

- 1 und 2 zeigen alle Datenbanken im MongoDB
- 3 setze ich verbindung zu einer Datenbank
- 4 und 5 zeigt mir alle Collections im Datenbank
